﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GoogleApi : MonoBehaviour
{

    public RawImage img;
    public InputField latText;
    public InputField longText;

    string url;

    public float lat;
    public float lon;

    LocationInfo li;

    public int zoom = 14;
    public int mapWidth = 640;
    public int mapHeight = 640;

    public enum mapType { roadmap, satellite, hybrid, terrain }
    public mapType mapSelected;
    public int scale;


    public IEnumerator Map()
    {
        url = "https://maps.googleapis.com/maps/api/staticmap?center=" + lat + "," + lon +
            "&zoom=" + zoom + "&size=" + mapWidth + "x" + mapHeight + "&scale=" + scale
            + "&maptype=" + mapSelected +
            "&markers=color:blue%7Clabel:S%7C40.702147,-74.015794&markers=color:green%7Clabel:G%7C40.711614,-74.012318&markers=color:red%7Clabel:C%7C40.718217,-73.998284&key=AIzaSyDKmDOZos0WiU56pJlniB6mNaVSzexmHSM"
            + "&markers=color:red%7Clabel:C%7C" + lat + "," + lon;
        WWW www = new WWW(url);
        yield return www;
        img.texture = www.texture;
        img.SetNativeSize();

    }
    // Use this for initialization
    void Start()
    {
        ResetLoc();

        //img = gameObject.GetComponent<RawImage> ();
        mapWidth = (int)img.rectTransform.sizeDelta.x;
        mapHeight = (int)img.rectTransform.sizeDelta.y;

        latText.onValueChanged.AddListener((value) => OnLatChange(value));
        longText.onValueChanged.AddListener((value) => OnLonChange(value));

        ResetLoc();

        StartCoroutine(Map());
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ResetLoc()
    {
        GPS.Point curPoint = GPS.Instance.getCurrentLocationPoint();
        lat = (float)curPoint.x;
        lon = (float)curPoint.y;

        latText.text = lat.ToString();
        longText.text = lon.ToString();

        this.PostNotification(TimePanelController.OnTimeChange);
    }

    private void OnLatChange(string value)
    {
        lat = float.Parse(value);
        StartCoroutine(Map());
        this.PostNotification(TimePanelController.OnTimeChange);
    }

    private void OnLonChange(string value)
    {
        lon = float.Parse(value);
        StartCoroutine(Map());
        this.PostNotification(TimePanelController.OnTimeChange);
    }

    public void latMax()
    {
        lat += 1;
        latText.text = lat.ToString();
    }

    public void latMin()
    {
        lat -= 1;
        latText.text = lat.ToString();
    }

    public void lonMax()
    {
        lon += 1;
        longText.text = lon.ToString();
    }

    public void lonMin()
    {
        lon -= 1;
        longText.text = lon.ToString();
    }
}
