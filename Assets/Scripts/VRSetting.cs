
using UnityEngine;

public class VRSetting : MonoBehaviour
{
    private Vector3 startingPosition;

    void Start()
    {
        startingPosition = transform.localPosition;
    }

    public void Reset()
    {
        transform.localPosition = startingPosition;
    }

    public void Recenter()
    {
#if !UNITY_EDITOR
        GvrCardboardHelpers.Recenter();
#else
      if (GvrEditorEmulator.Instance != null) {
        GvrEditorEmulator.Instance.Recenter();
      }
#endif  // !UNITY_EDITOR
    }
}

