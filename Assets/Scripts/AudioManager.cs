﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    AudioSource audio;
    // Use this for initialization
    void Start()
    {
        audio = GetComponent<AudioSource>();
        audio.mute = true;
        StartCoroutine(WaitForLoading());
    }

    // Update is called once per frame
    void Update()
    {

    }

    private IEnumerator WaitForLoading()
    {
        yield return new WaitForSeconds(5);
        audio.mute = false;
    }
}
