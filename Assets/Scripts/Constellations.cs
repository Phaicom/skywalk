﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Constellations : MonoBehaviour
{
    public Transform rig;

    [SerializeField]
    GameObject groupPrefabs;
    [SerializeField]
    GameObject linePrefabs;
    [SerializeField]
    GameObject[] constPrefabs;
    [SerializeField]
    Material lineMat;

    public TextAsset groupGeoJSON;
    public TextAsset lineGeoJSON;

    public GeoJSON.FeatureCollection collection;

    private List<GameObject> goConsts;
    private List<GameObject> constellations;
    // Use this for initialization
    private void Start()
    {
        goConsts = new List<GameObject>();
        constellations = new List<GameObject>();

        LoadGroup();
        LoadStarAndLine();
        rig.transform.Rotate(new Vector3(-90, 0, 0));
    }

    private void Update()
    {
        foreach (GameObject goConst in goConsts)
        {
            goConst.transform.LookAt(Camera.main.transform);
        }
    }

    private void LoadGroup()
    {
        collection = GeoJSON.GeoJSONObject.Deserialize(groupGeoJSON.text);
        int counter = 0;
        foreach (var feature in collection.features)
        {
            GameObject group = new GameObject("Group");
            group.transform.SetParent(rig.transform);
            constellations.Add(group.gameObject);

            Debug.Log("type: " + feature.type);
            Debug.Log("geometry type: " + feature.geometry.type);

            foreach (var position in feature.geometry.AllPositions())
            {
                Debug.Log(position.latitude + ":" + position.longitude);
                var dir = Quaternion.AngleAxis(position.longitude, -Vector3.up) * Quaternion.AngleAxis(position.latitude, -Vector3.right) * new Vector3(0, 0, 500);
                Debug.Log("longtitude: " + dir);
                GameObject go = Instantiate(groupPrefabs, dir, Quaternion.identity);
                go.transform.SetParent(group.transform);

                // generate cont sprite
                var constDir = Quaternion.AngleAxis(position.longitude, -Vector3.up) * Quaternion.AngleAxis(position.latitude, -Vector3.right) * new Vector3(0, 0, 400);
                GameObject goConst = Instantiate(constPrefabs[counter], constDir, Quaternion.identity);
                goConst.transform.localScale = new Vector3(20, 20, 20);
                goConst.transform.SetParent(group.transform);
                goConsts.Add(goConst);
            }
            foreach (var property in feature.properties)
            {
                Debug.Log(property.Value + ", " + property.Key);
                if (property.Key.ToLower() == "name")
                {
                    group.name = property.Value;
                }
            }
            counter++;
        }
    }

    private void LoadStarAndLine()
    {
        int counter = 0;
        collection = GeoJSON.GeoJSONObject.Deserialize(lineGeoJSON.text);
        foreach (var feature in collection.features)
        {
            GameObject con = constellations[counter];

            GameObject group = new GameObject("StarsAndLine");
            group.transform.SetParent(con.transform);

            Debug.Log("type: " + feature.type);
            Debug.Log("geometry type: " + feature.geometry.type);

            List<Vector3> lines = new List<Vector3>();
            foreach (var position in feature.geometry.AllPositions())
            {
                Debug.Log(position.latitude + ":" + position.longitude);
                var dir = Quaternion.AngleAxis(position.longitude, -Vector3.up) * Quaternion.AngleAxis(position.latitude, -Vector3.right) * new Vector3(0, 0, 500);
                GameObject go = Instantiate(linePrefabs, dir, Quaternion.identity);
                float ranSize = Random.Range(1f, 3f);
                go.transform.localScale = new Vector3(ranSize, ranSize, ranSize);
                go.transform.SetParent(group.transform);
                lines.Add(dir);
            }

            // draw line
            for (int i = 1; i < lines.Count; i++)
            {
                Vector3 p = lines[i - 1];
                Vector3 c = lines[i];

                LineDrawer lineDrawer = new LineDrawer();
                lineDrawer.DrawLineInGameView(p, c, new Color(0.56f, 0.92f, 0.75f));
                lineDrawer.SetMaterial(lineMat);
                lineDrawer.lineObj.transform.SetParent(group.transform);
            }

            foreach (var property in feature.properties)
            {
                Debug.Log(property.Value + ", " + property.Key);
            }
            counter++;
        }
    }
}
