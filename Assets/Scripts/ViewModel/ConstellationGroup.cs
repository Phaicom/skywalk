﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using TheLiquidFire.Animation;

public class ConstellationGroup : MonoBehaviour
{
    public Button constButton;
    public GameObject constSprite;
    public GameObject Stars;
    public GameObject Lines;
    public SpriteRenderer videoSprite;

    private Vector3 defalutScale;
    private VideoPlayer videoPlayer;
    // Use this for initialization
    void Start()
    {
        videoPlayer = videoSprite.gameObject.GetComponent<VideoPlayer>();

        // set star scale to default/2
        defalutScale = constSprite.transform.localScale;
        constSprite.transform.localScale = new Vector3(defalutScale.x / 2, defalutScale.y / 2, defalutScale.z / 2);

        // set active gameObject
        constSprite.SetActive(false);
        Lines.SetActive(false);
        constButton.onClick.AddListener(() => SetGazeAt(true));
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnEnable()
    {

        this.AddObserver(OnGazeExit, EyeRaycaster.OnGazeExit);
    }

    private void OnDisable()
    {

        this.RemoveObserver(OnGazeExit, EyeRaycaster.OnGazeExit);
    }

    private void OnGazeExit(object sender, object args)
    {
        SetGazeAt(false);
    }

    public void SetGazeAt(bool gazeAt)
    {
        Debug.Log("SetGazeAt");
        //constSprite.SetActive(gazeAt);
        //Lines.SetActive(gazeAt);
        if (gazeAt)
        {
            StartCoroutine(Show());
        }
        else
        {
            StartCoroutine(Hide());
        }
    }

    private IEnumerator Show()
    {
        constSprite.SetActive(true);
        Lines.SetActive(true);
        videoPlayer.Play();

        Tweener tween = null;
        tween = constSprite.transform.ScaleTo(defalutScale, 1f);
        while (tween != null)
        {
            yield return null;
        }

        Color videoColor = videoSprite.color;
        for (float i = 0; i <= 1; i += 0.1f)
        {
            yield return null;
            videoColor.a = i;
            videoSprite.color = videoColor;
        }

        videoColor.a = 1;
        videoSprite.color = videoColor;
        yield return null;

        //videoPlayer.Play();
    }

    private IEnumerator Hide()
    {
        //Tweener tween = null;
        //tween = constSprite.transform.ScaleTo(new Vector3(defalutScale.x / 2, defalutScale.y / 2, defalutScale.z / 2));
        //while (tween.IsPlaying)
        //{
        //    yield return null;
        //}

        //Color videoColor = videoSprite.color;
        //for (float i = 1; i >= 0; i -= 0.1f)
        //{
        //    videoColor.a = i;
        //    videoSprite.color = videoColor;
        //    yield return null;
        //}

        //videoColor.a = 0;
        //videoSprite.color = videoColor;
        yield return null;

        constSprite.transform.localScale = new Vector3(defalutScale.x / 2, defalutScale.y / 2, defalutScale.z / 2);
        Color videoColor = videoSprite.color;
        videoColor.a = 0;
        videoSprite.color = videoColor;

        constSprite.SetActive(false);
        Lines.SetActive(false);
        videoPlayer.Stop();
    }
}
