﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingController : MonoBehaviour
{

    public const string WillSetting = "SettingController.OnSetting";
    public const string WillDateTime = "SettingController.OnDateTime";
    public const string WillLoc = "SettingController.OnLoc";

    public MainSettingController mainPanel;
    public DateTimeSettingController dateTimePanel;
    public LocationSettingController locPanel;

    // Use this for initialization
    void Start()
    {
        mainPanel.exit.onClick.AddListener(() => OnExit());
        dateTimePanel.exit.onClick.AddListener(() => OnExit());
        locPanel.exit.onClick.AddListener(() => OnExit());
        mainPanel.dateTime.onClick.AddListener(() => OnDateTime());
        mainPanel.location.onClick.AddListener(() => OnLocation());
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnExit()
    {
        mainPanel.gameObject.SetActive(false);
        dateTimePanel.gameObject.SetActive(false);
        locPanel.gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        this.AddObserver(OnSetting, WillSetting);
        this.AddObserver(OnDateTime, WillDateTime);
        this.AddObserver(OnLocation, WillLoc);
    }

    private void OnDisable()
    {
        this.RemoveObserver(OnSetting, WillSetting);
        this.RemoveObserver(OnDateTime, WillDateTime);
        this.RemoveObserver(OnLocation, WillLoc);
    }


    private void OnSetting(object sender, object args)
    {
        mainPanel.gameObject.SetActive(true);
    }

    private void OnDateTime(object sender, object args)
    {
        mainPanel.gameObject.SetActive(true);
        dateTimePanel.gameObject.SetActive(true);
    }

    private void OnDateTime()
    {
        dateTimePanel.gameObject.SetActive(true);
    }

    private void OnLocation(object sender, object args)
    {
        mainPanel.gameObject.SetActive(true);
        locPanel.gameObject.SetActive(true);
    }

    private void OnLocation()
    {
        locPanel.gameObject.SetActive(true);
    }
}
