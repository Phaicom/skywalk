﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TheLiquidFire.Animation;
using UnityEngine.XR;

public class UIController : MonoBehaviour
{
    public DefaultPanelController defaultPanel;
    public TimePanelController timePabel;
    public BottomPanelController bottomPanel;
    public SettingController settingPanel;

    public Camera playerCamera;

    float height;
    Vector3 showPos;
    Vector3 hidePos;
    bool isDefault = true;
    // Use this for initialization
    void Start()
    {
        height = Screen.height;
        defaultPanel.dateTime.onClick.AddListener(() => OpenTimePanel());
        timePabel.close.onClick.AddListener(() => OpenDefaultPanel());
        bottomPanel.resetCamera.onClick.AddListener(() => ResetCamera());
        SetTranPos();
    }

    private void ResetCamera()
    {
        StartCoroutine(RotateCamera());
    }

    private IEnumerator RotateCamera()
    {
        Tweener tween = null;

        tween = playerCamera.transform.RotateToLocal(new Vector3(0, 0, 0));

        while (tween != null)
        {
            yield return null;
        }

        yield return null;

        ResetCameras();
    }

    private void ResetCameras()
    {
        for (int i = 0; i < Camera.allCameras.Length; i++)
        {
            Camera cam = Camera.allCameras[i];
            if (cam.enabled && cam.stereoTargetEye != StereoTargetEyeMask.None)
            {
                cam.transform.localRotation = Quaternion.identity;
                cam.transform.localPosition = Vector3.zero;
                cam.ResetAspect();
            }
        }
    }

    private void OpenTimePanel()
    {
        // set default time
        timePabel.setDateTime = DatePickerControl.DateGlobal;
        this.PostNotification(TimePanelController.OnTimeChange, timePabel.setDateTime);

        isDefault = false;
        StartCoroutine(MovePanel(defaultPanel.transform, hidePos));
        StartCoroutine(MovePanel(timePabel.transform, showPos));
    }

    private void OpenDefaultPanel()
    {
        isDefault = true;
        StartCoroutine(MovePanel(timePabel.transform, hidePos));
        StartCoroutine(MovePanel(defaultPanel.transform, showPos));
    }

    private IEnumerator MovePanel(Transform panel, Vector3 pos)
    {
        Tweener tween = null;
        Debug.Log("pos.y: " + pos.y);
        tween = panel.MoveToLocal(new Vector3(0, pos.y, 0));
        while (tween.IsPlaying)
        {
            yield return null;
        }
    }

    private void Update()
    {
        if (height != Screen.height)
        {
            Debug.Log("orientation");
            height = Screen.height;
            SetTranPos();
        }
    }

    private void SetTranPos()
    {
        if (isDefault)
        {
            showPos = defaultPanel.transform.localPosition;
            hidePos = timePabel.transform.localPosition;
            //SetSize();
        }
        else
        {

            hidePos = defaultPanel.transform.localPosition;
            showPos = timePabel.transform.localPosition;
        }
    }
}
