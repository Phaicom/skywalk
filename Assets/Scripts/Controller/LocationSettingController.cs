﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LocationSettingController : MonoBehaviour
{
    public Button exit;
    public Button back;
    public Toggle nowLoc;
    public Toggle selectedLoc;
    public GoogleApi locationPicker;


    // Use this for initialization
    void Start()
    {
        back.onClick.AddListener(() => OnBack());
        nowLoc.onValueChanged.AddListener((value) => OnNowChange(value));
        selectedLoc.onValueChanged.AddListener((value) => OnSelectedChange(value));
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnBack()
    {
        gameObject.SetActive(false);
    }

    private void OnNowChange(bool value)
    {
        selectedLoc.isOn = !value;
        locationPicker.gameObject.SetActive(selectedLoc.isOn);
        if (nowLoc.isOn)
            locationPicker.ResetLoc();

        StartCoroutine(locationPicker.Map());
        this.PostNotification(TimePanelController.OnTimeChange);
    }

    private void OnSelectedChange(bool value)
    {
        nowLoc.isOn = !value;
        locationPicker.gameObject.SetActive(selectedLoc.isOn);
        if (nowLoc.isOn)
            locationPicker.ResetLoc();

        StartCoroutine(locationPicker.Map());
        this.PostNotification(TimePanelController.OnTimeChange);
    }
}
