﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DateTimeSettingController : MonoBehaviour
{
    public Button exit;
    public Button back;
    public Toggle nowTime;
    public Toggle selectedTime;
    public DatePickerControl dateTimePicket;

    public TimePanelController timePanel;

    // Use this for initialization
    void Start()
    {
        back.onClick.AddListener(() => OnBack());
        nowTime.onValueChanged.AddListener((value) => OnNowChange(value));
        selectedTime.onValueChanged.AddListener((value) => OnSelectedChange(value));
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnBack()
    {
        gameObject.SetActive(false);
    }

    private void OnNowChange(bool value)
    {
        selectedTime.isOn = !value;
        dateTimePicket.gameObject.SetActive(selectedTime.isOn);

        if (nowTime.isOn)
            dateTimePicket.fechaHoy();

        timePanel.setDateTime = dateTimePicket.fecha;
        this.PostNotification(TimePanelController.OnTimeChange, timePanel.setDateTime);
    }

    private void OnSelectedChange(bool value)
    {
        nowTime.isOn = !value;
        dateTimePicket.gameObject.SetActive(selectedTime.isOn);

        if (nowTime.isOn)
            dateTimePicket.fechaHoy();

        timePanel.setDateTime = dateTimePicket.fecha;
        this.PostNotification(TimePanelController.OnTimeChange, timePanel.setDateTime);
    }

    public void OnValueChangeRotate()
    {
        timePanel.setDateTime = DatePickerControl.DateGlobal;
        this.PostNotification(TimePanelController.OnTimeChange, timePanel.setDateTime);
    }
}
