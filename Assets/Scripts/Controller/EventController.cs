﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class EventController : MonoBehaviour
{
    public EventSystem normalEvent;
    public EventSystem vrEvent;


    // Use this for initialization
    void Start()
    {
        normalEvent.gameObject.SetActive(true);
        vrEvent.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
