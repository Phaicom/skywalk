﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimePanelController : MonoBehaviour
{
    public const string OnTimeChange = "TimePanelController.OnTimeChange";

    public Button timeSetting;
    public Button back;
    public Button pause;
    public Button next;
    public Button resetTime;
    public Text showDate;
    public Button close;
    public Text showTime;

    public DatePickerControl dpc;

    // time to calculate degree
    public System.DateTime setDateTime;
    // Use this for initialization
    void Start()
    {
        dpc.fechaHoy();
        setDateTime = DatePickerControl.DateGlobal;
        back.onClick.AddListener(() => InputTime(-60));
        next.onClick.AddListener(() => InputTime(60));
        resetTime.onClick.AddListener(() => ResetTime());

        timeSetting.onClick.AddListener(() => OnTimeSetting());
    }

    // Update is called once per frame
    void Update()
    {
        showDate.text = setDateTime.ToString("dd/MM/yy");
        showTime.text = setDateTime.ToString("HH:mm:ss");
    }

    private void OnTimeSetting()
    {
        this.PostNotification(SettingController.WillDateTime);
    }

    private void InputTime(double time)
    {
        setDateTime = setDateTime.AddMinutes(time);
        this.PostNotification(OnTimeChange, setDateTime);
    }

    private void ResetTime()
    {
        dpc.fechaHoy();
        setDateTime = DatePickerControl.DateGlobal;
        this.PostNotification(OnTimeChange, setDateTime);
    }
}
