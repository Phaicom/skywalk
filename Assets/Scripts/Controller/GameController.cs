﻿using System;
using System.Collections;
using System.Collections.Generic;
using TheLiquidFire.Animation;
using UnityEngine;
using UnityEngine.XR;

public class GameController : MonoBehaviour
{
    [SerializeField]
    UIController uiController;
    [SerializeField]
    FloorController floorController;
    [SerializeField]
    EventController eventController;
    [SerializeField]
    SkySphereController sphereController;

    // Use this for initialization
    private void Start()
    {
        uiController.defaultPanel.vrMode.onClick.AddListener(() => ToggleVR());
        floorController.exitVr.onClick.AddListener(() => ToggleVR());
    }

    private void ToggleVR()
    {
        if (XRSettings.loadedDeviceName == "cardboard")
        {
            floorController.gameObject.SetActive(false);
            StartCoroutine(LoadDevice(""));
        }
        else
        {
            floorController.gameObject.SetActive(true);
            StartCoroutine(LoadDevice("cardboard"));
        }
    }

    private IEnumerator LoadDevice(string newDevice)
    {
        XRSettings.LoadDeviceByName(newDevice);
        yield return null;

        if (newDevice == "cardboard")
        {
            XRSettings.enabled = true;
            uiController.gameObject.SetActive(false);
        }
        else
        {
            ResetCameras();
            XRSettings.enabled = true;
            uiController.gameObject.SetActive(true);
        }
    }

    private void ResetCameras()
    {
        for (int i = 0; i < Camera.allCameras.Length; i++)
        {
            Camera cam = Camera.allCameras[i];
            if (cam.enabled && cam.stereoTargetEye != StereoTargetEyeMask.None)
            {
                cam.transform.localRotation = Quaternion.identity;
                cam.transform.localPosition = Vector3.zero;
                cam.ResetAspect();
            }
        }
    }

    private void OnEnable()
    {
        this.AddObserver(RotateSphere, TimePanelController.OnTimeChange);
    }

    private void OnDisable()
    {
        this.RemoveObserver(RotateSphere, TimePanelController.OnTimeChange);
    }

    private void RotateSphere(object sender, object args)
    {
        StartCoroutine(TranRotateSphere());
    }

    private IEnumerator TranRotateSphere()
    {
        yield return null;

        // time
        long dateTimeTick = uiController.timePabel.setDateTime.Ticks;
        // loc
        float lat = uiController.settingPanel.locPanel.locationPicker.lat;
        float lon = uiController.settingPanel.locPanel.locationPicker.lon;

        TimeSpan elapsedSpan = new TimeSpan(dateTimeTick);
        float totalLoc = (lat / 90) * (lon / 180);
        float degree = (float)(elapsedSpan.TotalHours % 24) * (180 / 24) * (totalLoc == 0 ? 1 : totalLoc);
        Tweener tween = null;

        if (sphereController.constellations.transform.localEulerAngles.z != degree)
        {
            tween = sphereController.constellations.transform.RotateToLocal(new Vector3(0, 0, degree));
        }
        else
        {
            // dont transform
        }

        while (tween != null)
        {
            yield return null;
        }
    }
}
