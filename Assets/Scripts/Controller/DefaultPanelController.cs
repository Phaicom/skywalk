﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DefaultPanelController : MonoBehaviour
{

    public Button setting;
    public Button vrMode;
    public Button dateTime;

    Text localTime;
    // Use this for initialization
    void Start()
    {
        localTime = dateTime.gameObject.GetComponent<Text>();
        setting.onClick.AddListener(() => OnSetting());
    }

    // Update is called once per frame
    void Update()
    {
        System.DateTime dt = System.DateTime.UtcNow.AddHours(7);
        localTime.text = dt.ToString("HH:mm:ss");
    }

    private void OnSetting()
    {
        this.PostNotification(SettingController.WillSetting);
    }
}
